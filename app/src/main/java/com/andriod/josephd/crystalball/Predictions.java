package com.andriod.josephd.crystalball;

public class Predictions {

    public static Predictions predictions;
    private String[] answers;

    private Predictions(){
        answers = new String[] {
                "Your wishes will come true.",
                "Your wishes will NEVER come true."
        };
    }

    public static Predictions get() {
        if (predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        return answers[1];
    }
}
//couldnt figure out more than 3 challenges